package com.higgsup.repositories;/*
  By Chi Can Em  10/5/2017
 */


import com.higgsup.entities.product.Producer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProducerRepo extends PagingAndSortingRepository<Producer,Integer>,ProducerRepoCustom { }
