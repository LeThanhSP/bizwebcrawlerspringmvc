package com.higgsup.controller;/*
  By Chi Can Em  15-12-2017
 */

import com.higgsup.entities.product.Product;
import com.higgsup.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowListProductController extends AbstractController {
    @Autowired
    ProductServices productServices;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView modelAndView = new ModelAndView("ShowListProduct");
        List<Product> productList = productServices.getProduct();
        modelAndView.addObject("productList",productList);
        return modelAndView;
    }
}
