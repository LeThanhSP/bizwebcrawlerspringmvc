package com.higgsup.controller;/*
  By Chi Can Em  14-12-2017
 */

import com.higgsup.services.scheduling.StartSchedulingProduct;
import com.higgsup.utils.CommonUtil;
import com.higgsup.utils.SystemSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StartSchedulingProductControllter extends AbstractController {
    @Autowired
    StartSchedulingProduct startSchedulingProduct;
    private String nameThread = "startSchedulingProduct";
    private String startName = "start";
    private String stopName = "start";
    private String nameButtonStartScheduling = "ButtonStartScheduling";

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        String stt = "";
        ModelAndView modelAndView = new ModelAndView("StartSchedulingProduct");
        if (!CommonUtil.checkIsThread(nameThread)) {
            SystemSettings.getGetSystemSetting().setButtonStartScheduling(stopName);
            modelAndView.addObject(nameButtonStartScheduling, SystemSettings.getGetSystemSetting().getButtonStartScheduling());
            stt = StartSchedulingProduct();
            modelAndView.addObject("stt", stt);
        } else {
            httpServletResponse.sendRedirect("/admin/StatisticalControllter");
        }
        return modelAndView;
    }

    public String StartSchedulingProduct() {

        if (CommonUtil.checkIsThread(nameThread)) {
            return startName + " Thất Bại";
        } else {
            Thread thread1 = new Thread() {
                @Override
                public void run() {
                    startSchedulingProduct.startSchedulingProduct();
                }
            };
            thread1.setName(nameThread);
            thread1.start();
            return startName + " Thành Công";
        }
    }

    public String StopSchedulingProduct() {
        Thread thread = CommonUtil.getThreadByName(nameThread);
        if (thread != null) {
            thread.stop();
            return "Thành công Stop Lại";
        } else {
            return "Thất Bại";
        }
    }
}
