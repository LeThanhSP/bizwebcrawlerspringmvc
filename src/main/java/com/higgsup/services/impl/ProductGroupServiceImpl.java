package com.higgsup.services.impl;/*
  By Chi Can Em  10/5/2017
 */

import com.higgsup.entities.product.ProductGroup;
import com.higgsup.repositories.ProductGroupRepo;
import com.higgsup.services.ProductGroupServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductGroupServiceImpl implements ProductGroupServices {
    @Autowired
    ProductGroupRepo productGroupRepo;
    @Override
    public Integer getIDProductGroup(String name) {
        return productGroupRepo.getIDProductGroup(name);
    }

    @Override
    public void save(ProductGroup productGroup) {
        productGroupRepo.save(productGroup);
    }
}
