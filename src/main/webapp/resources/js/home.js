function formSubmit() {
    document.getElementById("logoutForm").submit();
}

function showlistproductcontroller() {
    $.ajax({
        url: "/showlistproductcontroller",
        type: "get",
        dataType: "text",
        success: function (result) {
            $('#result').html(result);
        },
        complete: function (xhr, textStatus) {
            if (xhr.status == 403) {
                $('#result').html("Bạn không có quyền xem");
                $('#stt').html("Bạn không có quyền xem");
            }
            if (xhr.status == 200) {
                $('#stt').html("lây dữ liệu thành công");
            }
        }
    });
}

function showlistproducercontroller() {
    $.ajax({
        url: "/showlistproducercontroller",
        type: "get",
        dataType: "text",
        success: function (result) {
            $('#result').html(result);
        },
        complete: function (xhr, textStatus) {
            if (xhr.status == 403) {
                $('#result').html("Bạn không có quyền xem");
                $('#stt').html("Bạn không có quyền xem");
            }
            if (xhr.status == 200) {
                $('#stt').html("lây dữ liệu thành công");
            }
        }
    });
}

function schedulingProduct() {
    $.ajax({
        url: "/admin/SchedulingProduct",
        type: "get",
        dataType: "text",
        success: function (result) {
            $('#result').html(result);
        },
        complete: function (xhr, textStatus) {
            if (xhr.status == 403) {
                $('#result').html("Bạn không có quyền xem");
                $('#stt').html("Bạn không có quyền xem");
            }
            if (xhr.status == 200) {
                $('#stt').html("lây dữ liệu thành công");
            }
        }
    });
}


function showStatistical() {
    $.ajax({
        url: "/admin/StatisticalControllter",
        type: "get",
        dataType: "text",
        success: function (result) {
            $('#result').html(result);
        },
        complete: function (xhr, textStatus) {
            if (xhr.status == 403) {
                $('#result').html("Bạn không có quyền xem");
                $('#stt').html("Bạn không có quyền xem");
            }
            if (xhr.status == 200) {
                $('#stt').html("lây dữ liệu thành công");
            }
        }
    });
}