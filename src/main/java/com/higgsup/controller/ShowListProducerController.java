package com.higgsup.controller;/*
  By Chi Can Em  15-12-2017
 */

import com.higgsup.entities.product.Producer;
import com.higgsup.services.ProducerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowListProducerController extends AbstractController {
    @Autowired
    ProducerServices producerServices;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView modelAndView = new ModelAndView("ShowListProducer");
        List<Producer> producerList = producerServices.listProducer();
        System.out.println(producerList);
        modelAndView.addObject("producerList", producerList);
        return modelAndView;
    }
}
