package com.higgsup.services.impl;/*
  By Chi Can Em  09-10-2017
 */


import com.higgsup.entities.product.Producer;
import com.higgsup.repositories.ProducerRepo;
import com.higgsup.services.ProducerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProducerServiceImpl implements ProducerServices {
    @Autowired
    ProducerRepo producerRepo;

    @Override
    public boolean hasProducerByName(String name) {
        return producerRepo.hasProducerByName(name);
    }

    @Override
    public Integer getIdProducerByName(String name) {
        return producerRepo.getIdProducerByName(name);
    }

    @Override
    public void save(Producer producer) {
        producerRepo.save(producer);
    }

    @Override
    public List<Producer> listProducer() {
        return producerRepo.listProducer();
    }
}
