package com.higgsup.services;/*
  By Chi Can Em  09-10-2017
 */


import com.higgsup.entities.product.Producer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProducerServices {
    boolean hasProducerByName(String name);

    Integer getIdProducerByName(String name);

    void save(Producer producer);

    List<Producer> listProducer();
}
