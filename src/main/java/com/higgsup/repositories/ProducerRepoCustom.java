package com.higgsup.repositories;/*
  By Chi Can Em  10/6/2017
 */

import com.higgsup.entities.product.Producer;

import java.util.List;

public interface ProducerRepoCustom {
    boolean hasProducerByName(String name);
    Integer getIdProducerByName(String name);
    List<Producer> listProducer();
}
