package com.higgsup.services.impl;/*
  By Chi Can Em  12-12-2017
 */

import com.higgsup.entities.customer.Administrator;
import com.higgsup.repositories.AdministratorRepo;
import com.higgsup.services.AdministratorServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministratorImpl implements AdministratorServices {
    @Autowired
    AdministratorRepo administratorRepo;

    @Override
    public Administrator findByEmailAndPassWord(String email, String password) {
        return administratorRepo.findByEmailAndPassWord(email, password);
    }
}
