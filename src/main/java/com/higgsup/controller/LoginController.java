package com.higgsup.controller;/*
  By Chi Can Em  12-12-2017
 */

import com.higgsup.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginController extends AbstractController {
    @Autowired
    ProductServices productServices;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView modelAndView = new ModelAndView("Login");
        System.out.println(productServices.findById("9323903"));
        return modelAndView;
    }
}
