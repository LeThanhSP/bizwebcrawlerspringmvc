package com.higgsup.services;/*
  By Chi Can Em  12-12-2017
 */

import com.higgsup.entities.customer.Administrator;
import org.springframework.stereotype.Service;

@Service
public interface AdministratorServices {
    Administrator findByEmailAndPassWord(String email, String password);

}
