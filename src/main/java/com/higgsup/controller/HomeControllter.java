package com.higgsup.controller;/*
  By Chi Can Em  13-12-2017
 */

import com.higgsup.utils.SystemSettings;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

public class HomeControllter extends AbstractController {
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView modelAndView = new ModelAndView("Home");
        modelAndView.addObject("title","xin chao bạn");
        modelAndView.addObject("ButtonStartScheduling", SystemSettings.getGetSystemSetting().getButtonStartScheduling());
        Date date=new Date();
        modelAndView.addObject("message",date);
        return modelAndView;
    }
}
