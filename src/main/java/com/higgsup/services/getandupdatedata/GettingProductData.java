package com.higgsup.services.getandupdatedata;


import com.higgsup.entities.product.*;
import com.higgsup.services.*;
import com.higgsup.services.authentication.HtmlData;
import com.higgsup.utils.CommonUtil;
import com.higgsup.utils.DividePage;
import com.higgsup.utils.RequestHeader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by viquynh
 */
@Component
public class GettingProductData {
    private static final Logger logger = LoggerFactory.getLogger(GettingProductData.class);
    private HtmlData authenticationGetRequest = new HtmlData();
    private static final String url = RequestHeader.urlWebsite + "/products?page=";
    private String htmlData;
    private Document getHTML;
    private String cookie;
    private int success = 0;
    private int failure = 0;
    private int update = 0;
    private int addNew = 0;
    private int grossProduct = 0;
    @Autowired
    ProductServices productServices;
    @Autowired
    ProductGroupServices productGroupServices;
    @Autowired
    ProducerServices producerServices;
    @Autowired
    ProductCategoryServices productCategoryServices;
    @Autowired
    CategoryProductServices categoryProductServices;

    private Product product;
    private ProductGroup productGroup;
    private Producer producer;
    private final int TIME_SLEEP = 8;

    public boolean getDataProductFromWeb(String htmlData, String cookie) throws IOException {
        this.cookie = cookie;
        this.htmlData = htmlData;
        startGettingProductData();
        return true;
    }

    private void resetStatistical() {
        this.setSuccess(0);
        this.setFailure(0);
        this.setUpdate(0);
        this.setAddNew(0);
        this.setGrossProduct(0);
    }

    private void startGettingProductData() {
        try {
            resetStatistical();
            int pages = takeQuantityPage();
            System.out.println(pages);
            for (int page = 1; page <= pages; page++) {
                authenticationGetRequest.connectURLAndTakeHTML(url + page, cookie);
                getHTML = Jsoup.parse(authenticationGetRequest.getHtmlData());
                if (getHTML.title().equals("Đăng nhập quản trị hệ thống")) {
                    throw new Error("Error cookie");
                }
                getDataProduct();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int takeQuantityPage() {
        DividePage dividePage = new DividePage();
        getHTML = Jsoup.parse(htmlData);
        dividePage.setDataCheckingFromWeb(getHTML);
        Elements getDataAllProducts = dividePage.getDataCheckingFromWeb();
        int allProducts = Integer.parseInt(CommonUtil.cutID(getDataAllProducts.text()));
        this.grossProduct = allProducts;
        dividePage.setPage(allProducts);
        allProducts = dividePage.getPage();
        return allProducts;
    }

    private void getDataProduct() {
        Elements getDataFromTrTags = getHTML.select("tbody tr");
        for (Element tags : getDataFromTrTags) {
            HashMap getDataCategoryProduct = new HashMap();//lấy danh mục mới, hot, sale
            Elements getDataFromAhrefTags = tags.select("td  a[href]");
            product = new Product();
            productGroup = new ProductGroup();
            producer = new Producer();
            product.setProductID(CommonUtil.cutID(getDataFromAhrefTags.get(0).attr("href")));
            product.setName(getDataFromAhrefTags.get(0).text());
            Elements getIMGProduct = tags.select("td  img[src]");
            product.setImg("https:" + getIMGProduct.get(0).attr("src"));
            Elements getDataFromPTags = tags.select("td p");
            product.setStork(CommonUtil.cutQuantity(getDataFromPTags.get(0).text()));
            productGroup.setName(getDataFromPTags.get(1).text());
            producer.setName(getDataFromPTags.get(2).text());
            System.out.println(product);
            try {
                getContentProduct(getDataFromTrTags, getDataCategoryProduct);
                TimeUnit.SECONDS.sleep(TIME_SLEEP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                failure++;
            }
        }
    }

    private void getContentProduct(Elements getDataFromTrTags, HashMap getDataCategoryProduct) throws IOException {
        String titleURL;
        authenticationGetRequest.connectURLAndTakeHTML(RequestHeader.urlWebsite + "/products/" + product.getProductID(), cookie);
        getHTML = Jsoup.parse(authenticationGetRequest.getHtmlData());
        titleURL = getHTML.title();
        if (titleURL.equals("Đăng nhập quản trị hệ thống")) {
            throw new Error("Error cookie");
        }
        Elements getDataFromDivRowTag = getHTML.select("div.row");
        if (getDataFromDivRowTag.size() > 0) {
            getDataFromTrTags = getDataFromDivRowTag.get(0).select("div.controls textarea[bind*=content]");

        } else {
            authenticationGetRequest.connectURLAndTakeHTML(RequestHeader.urlWebsite + "/products/" + product.getProductID(), cookie);
            getHTML = Jsoup.parse(authenticationGetRequest.getHtmlData());
            titleURL = getHTML.title();
            if (titleURL.equals("Đăng nhập quản trị hệ thống")) {
                throw new Error("Error cookie");
            }
            getDataFromDivRowTag = getHTML.select("div.row");

        }
        if (getDataFromTrTags.size() > 0) {
            product.setContent(getDataFromTrTags.get(0).text());
        }
        getDataFromTrTags = getDataFromDivRowTag.get(2).select("a[href]");
        for (int j = 1; j < getDataFromTrTags.size(); j++) {
            if (getDataFromTrTags.get(j).text().toString().length() > 0) {
                getDataCategoryProduct.put(CommonUtil.cutID(getDataFromTrTags.get(j).attr("href")), getDataFromTrTags.get(j).text());
            }
        }
        Elements checkVersionProduct = getHTML.select("div.row a[bind-event-click*=changeOptionNamesModal.show(]");
        saveAndUpdateProductData(checkVersionProduct, getDataFromDivRowTag, getDataCategoryProduct);
        success++;
    }

    public void saveAndUpdateProductData(Elements checkVersionProduct, Elements getDataFromDivRowTag, HashMap getDataCategoryProduct) {
        try {
            product.setDescription("");
            product.setWeight(0.0);
            if (productGroupServices.getIDProductGroup(productGroup.getName()) == null) {
                productGroupServices.save(productGroup);
            }
            productGroup.setProductGroupID(productGroupServices.getIDProductGroup(productGroup.getName()));
            product.setProductGroup(productGroup);
            if (!producerServices.hasProducerByName(producer.getName())) {
                producerServices.save(producer);
            }
            producer.setProducerID(producerServices.getIdProducerByName(producer.getName()));
            product.setProducer(producer);
            if (productServices.findById(product.getProductID()) == null) {
                setProductToDB(checkVersionProduct, getDataFromDivRowTag, getDataCategoryProduct);
            } else {
                updateProductToDB(checkVersionProduct, getDataFromDivRowTag, getDataCategoryProduct);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("saveAndUpdateProductData error ", e);
        }
    }

    private void updateProductToDB(Elements checkVersionProduct, Elements getDataFromDivRowTag, HashMap getDataCategoryProduct) {
        if (checkVersionProduct.size() >= 1) {
            product.setPrice(0.0);
        } else {
            String checkGetPrice = null;
            if (getDataFromDivRowTag.size() >= 7) {
                Elements getDataFromTrTags = getDataFromDivRowTag.get(7).select("div.controls input[value] ");
                checkGetPrice = getDataFromTrTags.get(0).attr("value");
            }
            if (checkGetPrice == null) {
                product.setPrice(0.0);
            } else {
                product.setPrice(Double.parseDouble(checkGetPrice));
            }
        }
        Product dataProducerFromProductID = productServices.getDataProductFromProductID(product.getProductID());
        if (!(dataProducerFromProductID.getName().equals(product.getName()) && String.valueOf(dataProducerFromProductID.getPrice()).equals(String.valueOf(product.getPrice())) && dataProducerFromProductID.getStork() == product.getStork() && dataProducerFromProductID.getContent().equals(product.getContent()) && dataProducerFromProductID.getImg().equals(product.getImg()) && String.valueOf(productGroup.getProductGroupID()).equals(String.valueOf(productGroupServices.getIDProductGroup(productGroup.getName()))) && String.valueOf(producer.getProducerID()).equals(String.valueOf(producerServices.getIdProducerByName(producer.getName()))))) {
            productServices.save(product);
        }
        List<String> listProductCateID = categoryProductServices.getListProductCateIdFormProductIdInCategoryProduct(product.getProductID());
        Set set = getDataCategoryProduct.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry mapEntry = (Map.Entry) i.next();
            ProductCategory productCategory = new ProductCategory(String.valueOf(mapEntry.getKey()), String.valueOf(mapEntry.getValue()));
            productCategoryServices.save(productCategory);
            int indexList = listProductCateID.indexOf(mapEntry.getKey());
            if (indexList >= 0) {
                listProductCateID.remove(indexList);
            } else {
                categoryProductServices.save(new CategoryProduct((String) mapEntry.getKey(), product.getProductID()));
            }
        }
        for (String productCateID : listProductCateID) {
            categoryProductServices.deleteCategoryProduct(productCateID, product.getProductID());
        }
        update++;
    }

    private void setProductToDB(Elements checkVersionProduct, Elements getDataFromDivRowTag, HashMap getDataCategoryProduct) {
        if (checkVersionProduct.size() >= 1) {
            product.setPrice(0.0);
            productServices.save(product);
        } else {
            String checkGetPrice = null;
            if (getDataFromDivRowTag.size() >= 7) {
                Elements getDataFromTrTags = getDataFromDivRowTag.get(7).select("div.controls input[value] ");
                checkGetPrice = getDataFromTrTags.get(0).attr("value");
            }
            if (checkGetPrice == null) {
                product.setPrice(0.0);
            } else {
                product.setPrice(Double.parseDouble(checkGetPrice));
            }
            productServices.save(product);
            Set set = getDataCategoryProduct.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry mapEntry = (Map.Entry) i.next();
                ProductCategory productCategory = new ProductCategory((String) mapEntry.getKey(), (String) mapEntry.getValue());
                if (productCategoryServices.findCategoryById((String) mapEntry.getKey()) == null) {
                    productCategoryServices.save(productCategory);
                }
                if (categoryProductServices.findIdByName((String) mapEntry.getKey(), product.getProductID()) == -1) {
                    categoryProductServices.save(new CategoryProduct((String) mapEntry.getKey(), product.getProductID()));
                }

            }
        }
        addNew++;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getFailure() {
        return failure;
    }

    public void setFailure(int failure) {
        this.failure = failure;
    }

    public int getUpdate() {
        return update;
    }

    public void setUpdate(int update) {
        this.update = update;
    }

    public int getAddNew() {
        return addNew;
    }

    public void setAddNew(int addNew) {
        this.addNew = addNew;
    }

    public int getGrossProduct() {
        return grossProduct;
    }

    public void setGrossProduct(int grossProduct) {
        this.grossProduct = grossProduct;
    }
}
