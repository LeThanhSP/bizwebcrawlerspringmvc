package com.higgsup.services.scheduling;/*
  By Chi Can Em  14-12-2017
 */

import com.higgsup.services.authentication.CheckingAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StartSchedulingProduct {
    @Autowired
    CheckingAuthentication checkingAuthentication;
    @Autowired
    QueryingProductInformation queryingProductInformation;


    public void startSchedulingProduct() {
        try {
            checkingAuthentication.doRequestTakeCookie();
            queryingProductInformation.startScheduling();
        } catch (Exception e) {

        }
    }

}
