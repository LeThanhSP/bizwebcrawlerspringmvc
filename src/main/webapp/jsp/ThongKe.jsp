<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table border="1">
    <tr>
        <td>success</td>
        <td>failure</td>
        <td>update</td>
        <td>addNew</td>
        <td>grossProduct</td>
    </tr>
    <tr>
        <td>${success}</td>
        <td>${failure}</td>
        <td>${update}</td>
        <td>${addNew}</td>
        <td>${grossProduct}</td>
    </tr>
</table>
</body>
</html>
