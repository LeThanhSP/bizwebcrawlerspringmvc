<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table border="1" >
    <tr>
        <td>producerID</td>
        <td>name</td>
    </tr>
    <c:forEach var="producer" items="${producerList}">
        <tr>
            <td>${producer.producerID}</td>
            <td>${producer.name}</td>


        </tr>
    </c:forEach>
</table>
</body>
</html>
