package com.higgsup.services.impl;/*
  By Chi Can Em  09-10-2017
 */


import com.higgsup.entities.product.ProductCategory;
import com.higgsup.repositories.ProductCategoryRepo;
import com.higgsup.services.ProductCategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryServices {
    @Autowired
    ProductCategoryRepo productCategoryRepo;
    @Override
    public ProductCategory findCategoryById(String product_cate_id) {
        return productCategoryRepo.findCategoryById(product_cate_id);
    }

    @Override
    public void save(ProductCategory productCategory) {
        productCategoryRepo.save(productCategory);
    }
}
