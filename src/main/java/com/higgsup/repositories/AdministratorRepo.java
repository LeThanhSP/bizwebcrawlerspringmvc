package com.higgsup.repositories;/*
  By Chi Can Em  12-12-2017
 */

import com.higgsup.entities.customer.Administrator;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepo extends PagingAndSortingRepository<Administrator, String> {
    @Query(value = "SELECT  * FROM  administrator WHERE email=:email AND pass_word=:password", nativeQuery = true)
    Administrator findByEmailAndPassWord(@Param("email") String email, @Param("password") String password);
    @Query(value = "SELECT * FROM administrator WHERE id=:id", nativeQuery = true)
    Administrator getAdministrator(@Param("id") String id);
}
