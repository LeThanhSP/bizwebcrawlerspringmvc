package com.higgsup.services;/*
  By Chi Can Em  09-10-2017
 */


import com.higgsup.entities.product.ProductCategory;
import org.springframework.stereotype.Service;

@Service
public interface ProductCategoryServices {
    ProductCategory findCategoryById(String product_cate_id);
    void save(ProductCategory productCategory);
}
