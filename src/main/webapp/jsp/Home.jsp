<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page session="true" contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
<header>
    <script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/home.js" type="text/javascript"></script>
</header>
<h1>Title : ${title}</h1>
<span style="font-size: 40px">Message :</span>
<span id="stt" style="font-size: 40px">${stt} </span>
<c:url value="/j_spring_security_logout" var="logoutUrl"/>
<form action="${logoutUrl}" method="post" id="logoutForm">
    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form>
<script>

</script>
<c:if test="${pageContext.request.userPrincipal.name != null}">
    <h2>
        Welcome : ${pageContext.request.userPrincipal.name} | <a
            href="javascript:formSubmit()"> Logout</a>
    </h2>
</c:if>
<button id="SchedulingProduct" onclick="schedulingProduct()">SchedulingProduct</button>
<button id="show list product" onclick="showlistproductcontroller()">SchedulingProduct</button>
<button id="show list producer" onclick="showlistproducercontroller()">SchedulingProduct</button>
<button id="show list producer" onclick="showStatistical()">Thống kê</button>

<p id="result"></p>


</body>
</html>