package com.higgsup.controller;/*
  By Chi Can Em  15-12-2017
 */

import com.higgsup.services.getandupdatedata.GettingProductData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StatisticalControllter extends AbstractController {
    @Autowired
    GettingProductData gettingProductData;
    private int success = 0;
    private int failure = 0;
    private int update = 0;
    private int addNew = 0;
    private int grossProduct = 0;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView modelAndView = new ModelAndView("ThongKe");
        getStatistical();
        modelAndView.addObject("success", success);
        modelAndView.addObject("failure", failure);
        modelAndView.addObject("update", update);
        modelAndView.addObject("addNew", addNew);
        modelAndView.addObject("grossProduct", grossProduct);
        return modelAndView;
    }

    private void getStatistical() {
        success = gettingProductData.getSuccess();
        failure = gettingProductData.getFailure();
        update = gettingProductData.getUpdate();
        addNew = gettingProductData.getAddNew();
        grossProduct = gettingProductData.getGrossProduct();

    }
}
