package com.higgsup.services.impl;/*
  By Chi Can Em  09-10-2017
 */


import com.higgsup.entities.product.CategoryProduct;
import com.higgsup.repositories.CategoryProductRepo;
import com.higgsup.services.CategoryProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryProductServiceImpl implements CategoryProductServices {
    @Autowired
    CategoryProductRepo categoryProductRepo;
    @Override
    public Integer findIdByName(String product_cate_id, String product_id) {
        return categoryProductRepo.findIdByName(product_cate_id,product_id);
    }

    @Override
    public List<String> getListProductCateIdFormProductIdInCategoryProduct(String id) {
        return categoryProductRepo.getListProductCateIdFormProductIdInCategoryProduct(id);
    }

    @Override
    public void deleteCategoryProduct(String productCateId, String productId) {
        categoryProductRepo.deleteCategoryProduct(productCateId,productId);
    }

    @Override
    public void save(CategoryProduct categoryProduct) {
        categoryProductRepo.save(categoryProduct);
    }
}
