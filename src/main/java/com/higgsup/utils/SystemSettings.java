package com.higgsup.utils;/*
  By Chi Can Em  14-12-2017
 */

public final class SystemSettings {
    private String buttonStartScheduling = "start";
    private static SystemSettings getSystemSetting;

    public static SystemSettings getGetSystemSetting() {
        if (getSystemSetting == null) {
            synchronized (SystemSettings.class) {
                getSystemSetting = new SystemSettings();
            }
        }
        return getSystemSetting;
    }

    private SystemSettings() {
    }

    public String getButtonStartScheduling() {
        return buttonStartScheduling;
    }

    public void setButtonStartScheduling(String buttonStartScheduling) {
        this.buttonStartScheduling = buttonStartScheduling;
    }
}
