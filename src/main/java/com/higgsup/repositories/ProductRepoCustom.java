package com.higgsup.repositories;/*
  By Chi Can Em  9/28/2017
 */


import com.higgsup.entities.product.Product;

import java.util.List;

public interface ProductRepoCustom {
    List<Product> getProduct();
}
